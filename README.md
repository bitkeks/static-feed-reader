# static feed reader

This tool fetches RSS and atom feeds, parses their meta data and entries and writes out the headlines to a simple HTML
page. Categories are supported and generate different HTML files.

## Usage

It's really simple: put some feed URLs into the `config.py` file and run `sfr.py`. See the `config.py.example` file to
get a feeling for the config. The tool gives extensive feedback through its logging output. After a successful run,
the `output` directory contains an `index.html` and the category HTML files. View the `index.html` in your browser, it
links to the other pages.

You might need a virtual Python environment to install the requirements via `python3 -m pip install -r requirements.txt`
.

For feeds that block the faked Firefox user-agent (e.g. Akamai, Cloudflare), use the `curl_domains` list.

## Data storage

All fetched feeds are stored in an sqlite3 data base called `sfr.db`. In it, two tables exist:

* `fetches`: meta data of feed downloads and the hash of the compressed content.
* `feed_blobs`: the compressed contents downloaded from the feed hosts, referenced by their hash value.

Through this split of fetch meta data and the blobs storage space is reduced, since feeds that have not changed since
the last fetch are only stored once. There will be feeds that never generate the same feed twice, always creating a new blob.

## License

Copyright 2022, Dominik Pataky <software+sfr@dpataky.eu>. Licensed under GPLv3
