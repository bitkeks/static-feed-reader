"""
Static feed reader
Fetches RSS and atom feeds, parses their meta data and entries and writes out the headlines to a simple HTML page.

Copyright (C) 2022, Dominik Pataky <software+sfr@dpataky.eu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import hashlib
import json
import logging
import pathlib
import queue
import sqlite3
import sys
import time
import zlib
from concurrent.futures import ThreadPoolExecutor
from queue import Queue

import feedparser
import requests
from jinja2 import Environment, FileSystemLoader, select_autoescape

import config

feed_urls_combined: list = []

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
loghandler = logging.StreamHandler()
loghandler.setLevel(logging.DEBUG)
logformat = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
loghandler.setFormatter(logformat)
logger.addHandler(loghandler)


class FetchedFeed:
    def __init__(self, headers: dict, contentblob: bytes, elapsed: float, url: str):
        self._headers = headers
        self._contentblob = contentblob
        self._elapsed = elapsed
        self._fetched_url = url
        self._creation_time = time.time()

    def __repr__(self):
        return "<FetchedFeed for {url} created at {ts}>".format(
            url=self.fetched_url,
            ts=self.created_at
        )

    @property
    def headers(self) -> dict:
        return self._headers

    @property
    def headers_str(self) -> str:
        return json.dumps(self._headers)

    @property
    def content_str(self) -> str:
        return self._contentblob.decode("utf-8")

    @property
    def compressed_content(self) -> bytes:
        return zlib.compress(self._contentblob, 7)

    @property
    def compressed_content_hash(self) -> str:
        return hashlib.sha256(self.compressed_content).hexdigest()

    @property
    def elapsed_seconds(self):
        return self._elapsed

    @property
    def fetched_url(self):
        return self._fetched_url

    @property
    def created_at(self) -> str:
        return datetime.datetime.fromtimestamp(self._creation_time).strftime("%Y%m%d%H%M%S")


def feed_fetch_job(url: str, ff_queue: Queue):
    headers = {
        "User-Agent": "User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:100.0) Gecko/20100101 Firefox/100.0"
    }

    if url[len("https://"):].startswith(config.curl_domains):
        headers["User-Agent"] = "curl/7.79.1"

    try:
        r = requests.get(url, headers=headers, timeout=5.0, allow_redirects=False)
    except requests.exceptions.ReadTimeout as ex:
        logger.error("requests.exceptions.ReadTimeout for {url}: {msg}".format(url=url, msg=ex))
        return False, url
    except requests.exceptions.ConnectTimeout as ex:
        logger.error("requests.exceptions.ConnectTimeout for {url}: {msg}".format(url=url, msg=ex))
        return False, url

    if r.is_redirect or r.is_permanent_redirect:
        logger.warning("Feed at URL {url} redirects to {location}".format(url=url, location=r.headers.get("location")))
        return False, url
    elif r.ok:
        logger.info("Successfully fetched {url}".format(url=url))
        ff = FetchedFeed(dict(r.headers), r.content, r.elapsed.total_seconds(), r.url)
        ff_queue.put(ff)
        return True, url
    else:
        logger.error("Fetching {url} failed: code {code}".format(url=url, code=r.status_code))
        return False, url


def setup_database(filename: str = "sfr.db"):
    if pathlib.Path(filename).exists():
        return

    conn = sqlite3.connect(filename)
    cur = conn.cursor()
    cur.execute('''CREATE TABLE fetches (
        headers TEXT, 
        compressed_content_hash TEXT,
        elapsed_seconds INT,
        fetched_url TEXT NOT NULL,
        created_at TEXT NOT NULL,
        PRIMARY KEY(fetched_url, created_at),
        FOREIGN KEY(compressed_content_hash) REFERENCES feed_blobs(compressed_content_hash)
    )''')

    cur.execute('''CREATE TABLE feed_blobs (
        compressed_content_hash TEXT,
        compressed_content BLOB,
        PRIMARY KEY(compressed_content_hash)
    )''')

    conn.commit()
    conn.close()


def fetch_feeds(db_filename: str = "sfr.db"):
    feeds = []

    url_queue = queue.Queue()
    ff_queue = queue.Queue()

    for url in feed_urls_combined:
        url_queue.put(url)

    jobs = []
    with ThreadPoolExecutor(max_workers=10) as exe:
        while not url_queue.empty():
            _url = url_queue.get()
            logger.debug("Starting executor job for url {}".format(_url))
            job = exe.submit(feed_fetch_job, _url, ff_queue)
            jobs.append(job)

    failed_urls = []
    for job in jobs:
        res = job.result()
        if res[0] is False:
            failed_urls.append(res[1])
    if failed_urls:
        logger.error("There were errors in feeds: {}".format(", ".join(failed_urls)))

    while not ff_queue.empty():
        feeds.append(ff_queue.get())

    dbconn = sqlite3.connect(db_filename)
    cur = dbconn.cursor()

    for feed in feeds:
        cc_hash = feed.compressed_content_hash
        cur.execute("SELECT compressed_content_hash FROM feed_blobs WHERE compressed_content_hash = :cc_hash", {
            "cc_hash": cc_hash
        })

        hit = cur.fetchone()
        if not hit:
            cur.execute("INSERT INTO feed_blobs VALUES (:compressed_content_hash, :compressed_content)", {
                "compressed_content_hash": cc_hash,
                "compressed_content": feed.compressed_content
            })

        cur.execute(
            "INSERT INTO fetches values (:headers, :compressed_content_hash, :elapsed_seconds, :fetched_url, "
            ":created_at)", {
                "headers": feed.headers_str,
                "compressed_content_hash": feed.compressed_content_hash,
                "elapsed_seconds": feed.elapsed_seconds,
                "fetched_url": feed.fetched_url,
                "created_at": feed.created_at
            })

    dbconn.commit()
    dbconn.close()


def parse_feeds(db_filename: str = "sfr.db") -> list:
    feeds = []

    dbconn = sqlite3.connect(db_filename)
    dbconn.row_factory = sqlite3.Row
    cur = dbconn.cursor()

    latest_feed_fetches = []
    for row in cur.execute("SELECT fetched_url as fu, max(created_at) as ca FROM fetches GROUP BY fetched_url"):
        latest_feed_fetches.append((row["fu"], row["ca"]))

    for lff in latest_feed_fetches:
        c = cur.execute("SELECT fetched_url as url, compressed_content as blob "
                        "FROM fetches "
                        "JOIN feed_blobs USING(compressed_content_hash) "
                        "WHERE fetched_url = :fu AND created_at = :ca", {
                            "fu": lff[0],
                            "ca": lff[1]
                        })
        row = c.fetchone()

        if not row:
            logger.error("There was no row for {}".format(lff))

        if not row["blob"]:
            logger.error("Empty blob in row join")

        url = row["url"]
        if url not in feed_urls_combined:
            logger.debug("Skipping feed for URL {}".format(url))
            continue

        size_blob = len(row["blob"])
        time_decompression_start = time.time()
        decompressed = zlib.decompress(row["blob"])
        time_decompression_end = time.time()
        size_decompressed = len(decompressed)

        logger.debug(
            "Compression ratio for {url}: {ratio} (original {orig}, zlib {zlib}). Decompression took {time} seconds".format(
                url=url,
                ratio=round(size_blob / size_decompressed, 2),
                orig=size_decompressed,
                zlib=size_blob,
                time=round(time_decompression_end - time_decompression_start, 3)
            ))

        parsing_start = time.time()
        parsed = feedparser.parse(decompressed)
        parsing_dur = time.time() - parsing_start

        logger.debug("Feedparser.parse took {} seconds".format(round(parsing_dur, 3)))

        if parsed.bozo:
            logger.error("Error during parsing of decompressed body for URL {}".format(url))
            continue

        try:
            feeds.append({
                "title": parsed.feed.title,
                "updated": parsed.feed.updated if hasattr(parsed.feed, "updated") else None,
                "entries": parsed.entries[:config.entries_limit],
                "feed": parsed.feed,
                "url": url
            })
        except (KeyError, AttributeError) as ex:
            logger.error("Error {} during parsing of feed: {}".format(type(ex), ex))

    return feeds


def render_html():
    feeds = parse_feeds()
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

    env = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=select_autoescape()
    )
    template = env.get_template("feeds_overview.html")

    outdir = pathlib.Path("output")
    if not outdir.is_dir():
        outdir.mkdir(mode=0o755)

    cats = [cat for cat in config.feed_urls.keys()]

    index_cat2title = {}

    for category, urls in config.feed_urls.items():
        rend = template.render(feeds=[feed for feed in feeds if feed["url"] in urls],
                               timestamp=timestamp, feeds_category=category, categories=cats)
        output = pathlib.Path("output/{}.html".format(category))
        with output.open("w") as fh:
            fh.write(rend)

        logger.info("Output for category {} written to file".format(category))

    template = env.get_template("index.html")

    for cat, urls in config.feed_urls.items():
        index_cat2title[cat] = [feed["title"] for feed in feeds if feed["url"] in urls]

    rend = template.render(categories=cats, cat2title=index_cat2title)
    output = pathlib.Path("output/index.html")
    with output.open("w") as fh:
        fh.write(rend)


def run_main():
    global feed_urls_combined
    feed_urls_combined = [url for key, items in config.feed_urls.items() for url in items]

    setup_database()
    if "--skip-fetch" not in sys.argv:
        fetch_feeds()
    render_html()


run_main()
